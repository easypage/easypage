/**
 * jquery.easypage.js v0.0.1
 * http://git.oschina.net/easypage/easypage
 *
 * Copyright (C) 2016 ahhzd@vip.qq.com
 */
;(function($,window,document,undefined){

	var defaults = {prefix:'',pageSize:10,queryBtn:'',url:'',beforeLoad:function(){},loadComplete:function(){},fillTable:function(){},param:function(){}};

	var easyPage = function(ele,options){
		var _this = this;
		var element = ele;									//分页表格填充的DOM元素
		var settings = $.extend({},defaults,options); //将用户自定义的选型和默认选型进行合并
		settings.prefix = Math.random().toString(36).slice(-8);
		_this.init = function(){
			//创建分页页面下拉菜单列表
			var $pageSize_ = $("<select></select>");
			$pageSize_.attr("id","pageSize_"+settings.prefix).addClass("pageSize_select_");
			var $selectOption = null;
			if(!$.isArray(settings.pageSize)){settings.pageSize = [settings.pageSize];}
			$.each(settings.pageSize,function(i,n){
				$selectOption = $("<option></option>");
				$selectOption.attr("value",n).text(n);
				$pageSize_.append($selectOption);
			});
			var $pageInfo = $("<div></div>");
			$pageInfo.addClass("page_info_");
			var $pageNoInput = $("<input type='text'/>");
			$pageNoInput.addClass("jumpToNumber_").val(1).attr("id","currentPage_"+settings.prefix);
			var $totalPageInfo = $("<i></i>");
			$totalPageInfo.attr("id","totalPages_"+settings.prefix).html("1");
			var $totalRecordsInfo = $("<i></i>");
			$totalRecordsInfo.attr("id","totalRecords_"+settings.prefix).html("0");
			$pageInfo.append("第 ").append($pageNoInput).append(" 页，总 ").append($totalPageInfo).append("页，总").append($totalRecordsInfo).append("条，每页 ");
			$pageInfo.append($pageSize_);
			var $pageItemsContainer = $("<ul class='pageItemsContainer_'></ul>");
			var $preLink = $("<li><a href='javascript:void(0)' id='preBtn_"+settings.prefix+"' class='pagePre_'> &lt; </a></li>");
			var $nextLink = $("<li><a href='javascript:void(0)' id='nextBtn_"+settings.prefix+"' class='pageNext_'> &gt; </a></li>");
			var $pageContainer = $("<div id='page_"+settings.prefix+"' class='page_container_'></div>");
			$pageInfo.appendTo($pageContainer);//将分页数据信息插入分页DIV中
			$preLink.appendTo($pageItemsContainer);
			$nextLink.appendTo($pageItemsContainer);
			$pageItemsContainer.appendTo($pageContainer);
			$pageContainer.appendTo(element.parent());


			$("#preBtn_"+settings.prefix).bind("click",_this.prePage);
			$("#nextBtn_"+settings.prefix).bind("click",_this.nextPage);
			$(".jumpToNumber_").bind("blur",_this.loadData);
			$(".jumpToNumber_").bind("keydown",function(event){if(event.keyCode==13){_this.loadData();}});
			if(settings.queryBtn){$("#"+settings.queryBtn).bind("click",_this.loadData);}
			_this.loadData();
			return _this;
		}
		_this.loadPageInfo=function(){
			return {'pageSize':$("#pageSize_"+settings.prefix).val(),
				              'currentPage':$("#currentPage_"+settings.prefix).val(),
				              'totalRecords':$("#totalRecords_"+settings.prefix).html(),
							  'totalPages':$("#totalPages_"+settings.prefix).html()};
		}
		_this.fillPageItem=function(page){
			$("#page_"+settings.prefix+" .pageItems_li").remove();
			var pageNum=5,startPage=1,endPage=1;
			if(page.currentPage<=parseInt(pageNum/2)+1){
				startPage = 1;
				endPage	  = (page.totalPages>pageNum)?pageNum:page.totalPages;
			}
			if(page.currentPage>parseInt(pageNum/2)+1){
				startPage = page.currentPage - parseInt(pageNum/2);
				endPage   = page.currentPage + parseInt(pageNum/2);
				if(endPage>page.totalPages)endPage = page.totalPages;
			}
			var innerPage = "";
			while(startPage<=endPage){
				innerPage += ('<li class="pageItems_li '+(startPage == page.currentPage?'current':'')+'"><a href="javascript:;" class="pageItems_">'+startPage+'</a></li>');
				startPage++;
			}
			$("#preBtn_"+settings.prefix).parent().after(innerPage);
			$("#page_"+settings.prefix+" .pageItems_").bind("click",function(){
				$("#currentPage_"+settings.prefix).val(parseInt($(this).html()));
				_this.loadData();
			});
			$(".pageSize_select_").bind("change",function(){
				_this.loadData();
			});
		}
		_this.loadData=function(){
			var _op = settings.param(); //other params
			$.extend(_op,_this.loadPageInfo());
			_op['currentPage'] = (_op['currentPage']*1 > _op['totalPages']*1)?_op['totalPages']:_op['currentPage'];
			$.ajax({url:settings.url,type:"POST",data:_op,success:function(msg){
					if(msg&&msg.page){
						$("#pageSize_"+settings.prefix+" option[value='"+msg.page.pageSize+"']").attr("selected","selected");;
						$("#currentPage_"+settings.prefix).val(msg.page.currentPage);
						$("#totalRecords_"+settings.prefix).html(msg.page.totalRecords);
						$("#totalPages_"+settings.prefix).html(msg.page.totalPages);
						_this.fillPageItem(msg.page);
					}
					settings.fillTable(msg);
				},dataType:"json",beforeSend:settings.beforeLoad,complete:settings.loadComplete});
		}
		_this.nextPage=function(){
			var currentPage = parseInt($("#currentPage_"+settings.prefix).val());
			var totalPages   = parseInt($("#totalPages_"+settings.prefix).html());
			currentPage = ((currentPage+1)<=totalPages)?currentPage+1:totalPages;
			$("#currentPage_"+settings.prefix).val(currentPage);
			_this.loadData();
		}
		_this.prePage=function(){
			var currentPage = parseInt($("#currentPage_"+settings.prefix).val());
			currentPage = ((currentPage-1)>0)?currentPage-1:1;
			$("#currentPage_"+settings.prefix).val(currentPage);
			_this.loadData();
		}
		_this.refresh=function(){
			_this.loadData();
		}
	};

	//分页插件对外暴露接口
	$.fn.page = function(options){
		var ep = new easyPage(this,options);
		ep.init(ep);
		$.fn.refresh = function () {
			ep.loadData();
		}
		return this;
	};
})(jQuery,window,document);