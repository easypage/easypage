## easypage 使用手册

  **前言：** 我们在项目中不可避免的要使用分页功能；作者也使用过很多分页的插件；我们细细考虑一下可以发现；其实分页插件最最核
  心的功能就是：提供分页功能上一页、下一页、中间页码、调整到指定页，展示分页信息（当前页、总页数、总记录数、分页大小）、
  封装分页信息传递给后台！基于这个考虑作者决定开发一个专注于分页核心功能的插件；这样对比其他的分页js插件便具备更强的通用性。
  于是easypage诞生了！宗旨就是：让分页开发非常简单！
  
  **更新：** 20180831 进行了一次更新；本次更新向下兼容；主要更新时jquery.easypage.js；用户只需要下载下来覆盖您之前
  的文件即可！本次更新带来的新功能：
  1. 提供跳转到指定页功能；
  2. 提供自定义分页参数名称功能；
       

### 一、在项目中应用easypage
引入JS和CSS样式表：
```
<link href="css/skins/jquery.easypage.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="jquery.easypage.js"></script>
```

### 二、调用方法：
	var tableList = $(".pagin").page({
                 prefix:'cc', #前缀多个分页用前缀区分
                 #分页具体的action调用方法
                 url:'repairPackageAction!list.huzd',
                 #分页参数自定义（可选参数），我们会按照自定义的名称传递给处理层，0.0.2（20180831发布新增功能）
                 #默认参数是匹配struts2，并且需要一个Page.java pojo，用于struts2自动封装参数；
                 page:{'size':'page.pageSize','records':'page.totalRecords','current':'page.currentPage','pages':'page.totalPages'},
                 #默认的每页展示个数，分页列表；生产下拉菜单
                 pageSize:[10,5,20,30],
                 #页面查询按钮
                 queryBtn:'queryBtn',
                 #参数收集方法
                 param:getQueryParam,
                 #数据填充回调函数
                 fillTable:fillTable
       });
      新增刷新方法：
      tableList.refresh();
      自动刷新当前表格！


详细使用方法：
[使用详解](http://www.huzd.info/article/9)


![效果图](http://www.huzd.info/upload/2017/05/vnt43ech8si4spou4fhcb3rotc.jpg "效果图")


### 三、后续开发计划：
1. 增加分页记录数下拉菜单（已完成）；
2. 增加至少2套分页样式（50%）；
3. 后台传递参数可自定义（已完成）；
4. 添加跳转到指定页码（已完成）；